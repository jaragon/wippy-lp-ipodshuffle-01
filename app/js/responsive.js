$(function() {
  if ( ! $('html').hasClass('lt-ie8') ) {
  var $whatYouGet, 
      $enterEmail, 
      $paymentAside, 
      $paymentArticle,
      $aside,
      $article,
      $productImage

  enquire
  .register("screen and (max-width: 600px)", {
    setup: function() {
      $whatYouGet = $('.what-you-get');
      $enterEmail = $('.enter-email');
      $paymentAside = $('#payment-page > aside');
      $paymentArticle = $('#payment-page > article');
    },
    match: function() {
      $whatYouGet.insertAfter($enterEmail);
      $paymentAside.insertBefore($paymentArticle);
    },
    unmatch: function() {
      $whatYouGet.insertBefore($enterEmail);
      $paymentAside.insertAfter($paymentArticle);
    },
    deferSetup: true
  })
  .register("screen and (max-width: 400px)", {
    setup: function() {},
    match: function() {},
    unmatch: function() {},
    deferSetup: true
  });

  }

});
